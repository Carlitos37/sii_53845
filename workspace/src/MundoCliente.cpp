#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <unistd.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	
	munmap(pdatosMemoria, sizeof(pdatosMemoria));
	unlink("DatosMemoria");
	unlink("/tmp/logger_fifo");
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	////
	////
	////		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{

	char cad[200];
	//read(fifo_s_c, cad, sizeof(cad));
	socket_comunicacion.Receive(cad, sizeof(cad));
	sscanf(cad, "%f %f %f %f %f %f %f %f %f %f %f %d %d",
		&esfera.centro.x, &esfera.centro.y, &esfera.radio,
		&jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2,
		&jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2,
		&puntos1, &puntos2);
		
	
	pdatosMemoria -> esfera = esfera;
	pdatosMemoria -> raqueta1 = jugador1;

	if (pdatosMemoria->accion == -1)
		OnKeyboardDown('s',0,0);
	else if (pdatosMemoria->accion == 0){}
	else if (pdatosMemoria->accion == 1)
		OnKeyboardDown('w',0,0);



}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char tecla[]="0";
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}

}

void CMundoCliente::Init()
{
/*
	if((fifo_fd = open("/tmp/logger_fifo", O_WRONLY))<0)
	{
		perror("No puede abrirse el fifo");
		return;
	}
*/
	char IP[] = "127.0.0.1";
	char nombre[100];
	printf("Introduce tu nombre: ");
	scanf("%s", nombre);
	socket_comunicacion.Connect(IP,5000);
	socket_comunicacion.Send(nombre, sizeof(nombre));
	int fd_mmap;
	fd_mmap = open("DatosMemoria", O_CREAT|O_TRUNC|O_RDWR, 0666);
	if (fd_mmap < 0)
	{
		perror("Error creando fichero");
		return;
	}
	datosMemoria.esfera = esfera;
	datosMemoria.raqueta1=jugador1;
	datosMemoria.accion = 0;
	if (write(fd_mmap, &datosMemoria, sizeof(datosMemoria))<0)
	{
		perror("Error de escritura");
		return;
	}
	pdatosMemoria = static_cast <DatosMemCompartida*>(mmap(NULL, sizeof(datosMemoria), PROT_READ|PROT_WRITE,MAP_SHARED, fd_mmap, 0));
	if(pdatosMemoria == MAP_FAILED)
	{
		perror("Error proyeccion en memoria");
		close(fd_mmap);
		return;
	}
	close (fd_mmap);
/*if (mkfifo("/tmp/FIFO_servidor_cliente",0777)<0)
	{
		perror ("Error creando FIFO_servidor_cliente");
		return;
	}
	
	if ((fifo_s_c = open("/tmp/FIFO_servidor_cliente", O_RDONLY))<0)
	{
		perror("Error en la creacion de FIFO_servidor_cliente");
		return;
	}
	
	if (mkfifo("/tmp/FIFO_cliente_servidor",0777)<0)
	{
		perror ("Error creando FIFO_cliente_servidor");
		return;
	}
	
	if ((fifo_c_s = open("/tmp/FIFO_cliente_servidor", O_WRONLY))<0)
	{
		perror("Error en la creacion de FIFO_cliente_servidor");
		return;
	}

*/

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
