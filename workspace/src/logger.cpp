#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "Puntos.h"

int main()
{
	puntos puntuacion;
	int fifo_fd;

	if(mkfifo("/tmp/logger_fifo",0777) < 0)
	{
		perror("Fifo mal creada");
		return 1;
	}

	if((fifo_fd = open("/tmp/logger_fifo", O_RDONLY))<0)
	{
		perror("Error abriendo FIFO");
		return 1;
	}
	while(1){
	if(read(fifo_fd, &puntuacion, sizeof(puntuacion))>=sizeof(puntuacion))
	{
		if(puntuacion.ultimo_punto == 1) //jugador 1 marca 1 punto
		{
			printf("Jugador 1 marca 1 punto, lleva %d puntos\n", puntuacion.jug1);
		}

		if(puntuacion.ultimo_punto == 2) //jugador 2 marca 1 punto
		{
			printf("Jugador 2 marca 1 punto, lleva %d puntos\n", puntuacion.jug2);
		}
	}
	else if(read(fifo_fd, &puntuacion, sizeof(puntuacion)) == 0)
	{
		printf("El juego ha acabado\n");
		return 0;
	}
}

close(fifo_fd);
unlink("/tmp/logger_fifo");
printf("terminado");
return 0;

}
